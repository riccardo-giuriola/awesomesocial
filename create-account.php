<?php
include('./classes/DB.php');

if(isset($_POST["submit"]))
{
    $username = $_POST["username"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    $repeatpassword = $_POST["repeatpassword"];

    if($password == $repeatpassword){
        if(!DB::query('SELECT username FROM users WHERE username = :username', array(':username'=>$username))){
            if(strlen($username) >= 6 && strlen($username) <= 32){
                if(preg_match('/[a-zA-Z0-9_]+/', $username)){
                    if(strlen($password) >= 8 && strlen($password) <= 128){
                        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                            if(!DB::query('SELECT email FROM users WHERE email = :email', array(':email'=>$email))){
                                DB::query('INSERT INTO users VALUES (NULL, :username, :password, :email)', array(':username'=>$username, ':password'=>password_hash($password, PASSWORD_BCRYPT), ':email'=>$email));
                                echo "Success: User Created!";
                                header("location: login.php");
                            } else {
                                echo 'Error: An account with this email already exists!';
                            }
                        } else {
                            echo 'Error: Invalid email!';
                        }
                    } else {
                        echo 'Error: Password length must be between 8 and 128 characters!';
                    }
                } else {
                    echo 'Error: Username must include only letters and numbers!';
                }
            } else {
                echo 'Error: Username length must be between 6 and 32 characters!';
            }
        } else {
            echo 'Error: An account with this username already exists!';
        }
    } else {
        echo 'Error: The two passwords must match!';
    }
}

 ?>

<h1>Create Account</h1>
<form action="create-account.php" method="post">
  <input type="text" name="username" placeholder="Username"><p />
  <input type="email" name="email" placeholder="Email"><p />
  <input type="password" name="password" placeholder="Password"><p />
  <input type="password" name="repeatpassword" placeholder="Repeat Password"><p />
  <input type="submit" name="submit" value="Create Account">
</form><p/>
<a href="login.php">Login</a>
