<?php

class Comment{
    public static function createComment($commentBody, $postId, $userId){
        if(strlen($commentBody) > 160 || strlen($commentBody) < 1){
            die("Error: The comment length mustn't be longer than 160 characters!");
        }

        if(!DB::query('SELECT id FROM posts WHERE id = :postid', array(':postid'=>$postId))){
            echo "Error: Invalid post ID";
        } else {
            DB::query('INSERT INTO comments VALUES (NULL, :body, :userid, NOW(), :postid)', array(':body'=>$commentBody, ':userid'=>$userId, ':postid'=>$postId));
        }
    }

    public static function displayComments($postId, $location){
        $comments = DB::query('SELECT comments.comment, comments.id, comments.user_id, users.username FROM comments, users WHERE post_id = :postid AND comments.user_id = users.id', array(':postid'=>$postId));
        $output = "";
        foreach($comments as $comment){

            if($comment['user_id'] == Login::isLoggedIn()){
                $output .= "<form action=";
                if($location == "profile"){
                    $output .= "profile.php?username=".$_GET['username']."&commentid=".$comment['id'];
                }
                elseif ($location == "home") {
                    $output .= "index.php?commentid=".$comment['id'];
                }
                elseif ($location == "topics") {
                    $output .= "topics.php?topic=".$_GET['topic']."&commentid=".$comment['id'];
                }
                $output .= "' method='post'>
                ".$comment['comment']." - ".$comment['username']."
                <input type='submit' name='deletecomment' value='Delete Comment'>
                </form><p />";
            } else {
                $output .= $comment['comment']." - ".$comment['username'];
            }
        }
        return $output;
    }
}

 ?>
