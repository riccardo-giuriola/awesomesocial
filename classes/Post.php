<?php

class Post{
    public static function createPost($postbody, $loggedInUserId, $profileUserId){
        if(strlen($postbody) > 160 || strlen($postbody) < 1){
            die('Error: Post length mustn\'t be greater than 160 characters!');
        }
        $topics = self::getTopics($postbody);
        DB::query('INSERT INTO posts VALUES (NULL, :postbody, NOW(), :userid, 0, NULL, :topics)', array(':postbody'=>$postbody, ':userid'=>$profileUserId, ':topics'=>$topics));
    }

    public static function createImgPost($postbody, $loggedInUserId, $profileUserId, $postImg){
        if (strlen($postbody) > 160) {
                      die('Error: Post length mustn\'t be greater than 160 characters!');
              }
        $topics = self::getTopics($postbody);
        $username = DB::query("SELECT username FROM users WHERE id = :userid", array(':userid'=>Login::isLoggedIn()))[0]['username'];
        $target_dir = "img/".$username."/posts/";
        DB::query('INSERT INTO posts VALUES(NULL, :postbody, NOW(), :userid, 0, :postimg, :topics)', array(':postbody'=>$postbody, ':userid'=>$profileUserId, ':postimg'=>$target_dir.$postImg, ':topics'=>$topics));
    }

    public static function likePost($postId, $likerId){
        if (!DB::query('SELECT user_id FROM post_likes WHERE post_id=:postid AND user_id=:userid', array(':postid'=>$postId, ':userid'=>$likerId))) {
            DB::query('UPDATE posts SET likes=likes+1 WHERE id=:postid', array(':postid'=>$postId));
            DB::query('INSERT INTO post_likes VALUES (NULL, :postid, :userid)', array(':postid'=>$postId, ':userid'=>$likerId));
        } else {
            DB::query('UPDATE posts SET likes=likes-1 WHERE id=:postid', array(':postid'=>$postId));
            DB::query('DELETE FROM post_likes WHERE post_id=:postid AND user_id=:userid', array(':postid'=>$postId, ':userid'=>$likerId));
        }
    }

    public static function getTopics($text) {
            $text = explode(" ", $text);
            $topics = "";
            foreach ($text as $word) {
                    if (substr($word, 0, 1) == "#") {
                            $topics .= substr($word, 1).",";
                    }
            }
            return $topics;
        }

    public static function link_add($text) {
        $text = explode(" ", $text);
        $newstring = "";
        foreach ($text as $word) {
                if (substr($word, 0, 1) == "@") {
                        $newstring .= "<a href='profile.php?username=".substr($word, 1)."'>".htmlspecialchars($word)."</a> ";
                } else if (substr($word, 0, 1) == "#") {
                        $newstring .= "<a href='topics.php?topic=".substr($word, 1)."'>".htmlspecialchars($word)."</a> ";
                } else {
                        $newstring .= htmlspecialchars($word)." ";
                }
        }
        return $newstring;
    }

    public static function displayPosts($userid, $username, $loggedInUserId, $location){
        $action = "";
        if($location == "profile"){
            $dbposts = DB::query('SELECT * FROM posts WHERE user_id = :userid ORDER BY id DESC', array(':userid'=>$userid));
            $action = "profile.php?username=$username&postid=";
        }
        elseif ($location == "home") {
            $dbposts = DB::query('SELECT * FROM posts ORDER BY posted_at DESC', array(':loggedinuserid'=>$loggedInUserId));
            $action = "index.php?postid=";
        }
        elseif ($location == "topics") {
            $dbposts = DB::query('SELECT * FROM posts WHERE topics LIKE :topic ORDER BY posted_at DESC', array(':topic'=>'%'.$_GET['topic'].'%'));
            $action = "topics.php?topic=".$_GET['topic']."&postid=";
        }
        $posts = "";

        foreach($dbposts as $p){

            if(is_null($username)){
              $username = DB::query('SELECT username FROM users WHERE id = :userid', array(':userid'=>$p['user_id']))[0]['username'];
            }

            $posts .= "<h3><a href='profile.php?username=$username'>".$username."</a></h3>".
            self::link_add($p['body'])."<br />";
            if(!is_null($p['postimg'])){
                $posts .= "<img src='".$p['postimg']."' width='20%'><br />";
            }
            $posts .= "<span>".$p['likes']." likes</span><p />
            Posted on ".$p['posted_at']."<br />
            <form action='".$action.$p['id']."' method='post'>
            ";

            if(!DB::query('SELECT post_id FROM post_likes WHERE post_id=:postid AND user_id=:userid', array(':postid'=>$p['id'], ':userid'=>$loggedInUserId))){
                $posts .= "<input type='submit' name='likepost' value='Like' /> ";
            } else {
                $posts .= "<input type='submit' name='likepost' value='Unlike' /> ";
            }

            if ($p['user_id'] == $loggedInUserId){
                $posts .= "<input type='submit' name='deletepost' value='Delete Post' /><p />";
            }
            else {
                $posts .= "<p />";
            }

            $posts .= "<textarea name='commentbody' rows='4' cols='40'></textarea> ";
            $posts .= "<input type='submit' name='comment' value='Comment'>";
            $posts .= "
            </form><p />";
            $posts .= "Comments:<br />".Comment::displayComments($p['id'], $location)."
            <hr /><p />
            ";
            if($location == "home"){
                $username = null;
            }
        }
        echo $posts;
    }
}

 ?>
