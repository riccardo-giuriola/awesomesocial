<?php

class Image{
    public static function uploadImage($folder){
        $newFileName = "";
        $username = DB::query("SELECT username FROM users WHERE id = :userid", array(':userid'=>Login::isLoggedIn()))[0]['username'];
        $target_dir = "img/".$username."/".$folder."/";
        if(!file_exists($target_dir)){
            mkdir($target_dir, 0700, true);
        }
        $target_file = $target_dir . basename($_FILES["postimg"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
          $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
          if($check !== false) {
              echo "File is an image - " . $check["mime"] . ".";
              $uploadOk = 1;
          } else {
              echo "File is not an image.";
              $uploadOk = 0;
          }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
          echo "Sorry, file already exists.";
          $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["postimg"]["size"] > 10485760) {
          echo "Sorry, your file is too large.";
          $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
          echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
          $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
          echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
          if (move_uploaded_file($_FILES["postimg"]["tmp_name"], $target_file)) {
              echo "The file ". basename( $_FILES["postimg"]["name"]). " has been uploaded.";
              $newFileName = self::generateRandomString(16);
              $destination_img = $target_dir.$newFileName.".".str_replace("image/", "", $_FILES["postimg"]["type"]);
              self::compress($target_file, $destination_img, 60, $newFileName, $target_dir);

          } else {
              echo "Sorry, there was an error uploading your file.";
          }
        }
        if($newFileName == ""){
            return false;
        } else {
            return $newFileName.".".str_replace("image/", "", $_FILES["postimg"]["type"]);
        }
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function compress($source, $destination, $quality, $newFileName, $target_dir) {
        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);

        elseif ($info['mime'] == 'image/gif'){
          rename($source, $target_dir.$newFileName.".gif");
          return false;
        }

        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);

        imagejpeg($image, $destination, $quality);
        unlink($source);
    }
}

 ?>
