<?php
include('classes/DB.php');
include('classes/Login.php');

if(!Login::isLoggedIn()){
    die("Not logged in.");
} else {
    echo Login::isLoggedIn();
}

if(isset($_POST['logout'])){
    if(isset($_POST['alldevices'])){
        DB::query('DELETE FROM login_tokens WHERE user_id = :userid', array(':userid'=>Login::isLoggedIn()));
    } else {
          if(isset($_COOKIE['SNID'])){
              DB::query('DELETE FROM login_tokens WHERE token = :token', array(':token'=>sha1($_COOKIE['SNID'])));
          }
          setcookie('SNID', '1', time() - 3600);
          setcookie('SNID_', '1', time() - 3600);
    }
}

 ?>

<h1>Logout from your account</h1>
<p>Are you sure about that?</p>
<form action="logout.php" method="post">
  <input type="checkbox" name="alldevices" value="alldevices"> Logout from all devices<p />
  <input type="submit" name="logout" value="Logout">
</form>
