<?php
include('classes/DB.php');
include('classes/Login.php');
include('classes/Post.php');
include('classes/Comment.php');
include('classes/Image.php');

if (!Login::isLoggedIn()) {
  header("location: index.php");
} else {
  if(isset($_GET['topic'])){
    $loggedInUserId = Login::isLoggedIn();
    $username = DB::query("SELECT username FROM users WHERE id = :userid", array(':userid'=>$loggedInUserId))[0]['username'];
    echo "<a href='index.php'>Home</a>
          <a href='profile.php?username=$username'>My Profile</a><p />";
    echo "<h1>Topics: #".$_GET['topic']."</h1>";

    if (isset($_POST['deletepost'])) {
        if (DB::query('SELECT id FROM posts WHERE id=:postid AND user_id=:userid', array(':postid'=>$_GET['postid'], ':userid'=>$loggedInUserId))) {
            DB::query('DELETE FROM post_likes WHERE post_id=:postid', array(':postid'=>$_GET['postid']));
            DB::query('DELETE FROM comments WHERE post_id=:postid', array(':postid'=>$_GET['postid']));
            $deleteImg = DB::query("SELECT postimg FROM posts WHERE id = :postid", array(':postid'=>$_GET['postid']))[0]['postimg'];
            echo $deleteImg;
            DB::query('DELETE FROM posts WHERE id=:postid and user_id=:userid', array(':postid'=>$_GET['postid'], ':userid'=>$loggedInUserId));
            unlink($deleteImg);
            echo 'Post deleted!';
        }
    }

    if(isset($_GET['postid']) && !isset($_POST['deletepost']) && !isset($_POST['comment'])){
        Post::likePost($_GET['postid'], $loggedInUserId);
    }

    if(isset($_GET['postid']) && isset($_POST['comment'])){
        Comment::createComment($_POST['commentbody'], $_GET['postid'], $loggedInUserId);
    }

    if(isset($_POST['deletecomment'])){
        DB::query("DELETE FROM comments WHERE id = :commentId", array(':commentId'=>$_GET['commentid']));
    }

    Post::displayPosts(NULL, NULL, Login::isLoggedIn(), "topics");

  }
  else {
    header("location: index.php");
  }
}

 ?>
