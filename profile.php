<?php
include('classes/DB.php');
include('classes/Login.php');
include('classes/Post.php');
include('classes/Comment.php');
include('classes/Image.php');

$username = "";
if(!Login::isLoggedIn()){
    die("Not logged in.");
} else {
    echo "Logged userid: ".Login::isLoggedIn()."<p />";
    if(isset($_GET["username"])){
        if(DB::query('SELECT username FROM users WHERE username = :username', array(':username'=>$_GET["username"]))){
              $username = DB::query('SELECT username FROM users WHERE username = :username', array(':username'=>$_GET["username"]))[0]['username'];
              $userid = DB::query('SELECT id FROM users WHERE username = :username', array(':username'=>$_GET["username"]))[0]['id'];
              $loggedInUserId = Login::isLoggedIn();

              if(isset($_POST["post"])){
                  if($_FILES["postimg"]["size"] == 0){
                      Post::createPost($_POST["postbody"], $loggedInUserId, $userid);
                  } else {
                      $folder = "posts";
                      $postImg = Image::uploadImage($folder);
                      if(!empty($postImg)){
                            Post::createImgPost($_POST["postbody"], $loggedInUserId, $userid, $postImg);
                      }
                  }
              }

              if (isset($_POST['deletepost'])) {
                  if (DB::query('SELECT id FROM posts WHERE id=:postid AND user_id=:userid', array(':postid'=>$_GET['postid'], ':userid'=>$loggedInUserId))) {
                      DB::query('DELETE FROM post_likes WHERE post_id=:postid', array(':postid'=>$_GET['postid']));
                      DB::query('DELETE FROM comments WHERE post_id=:postid', array(':postid'=>$_GET['postid']));
                      $deleteImg = DB::query("SELECT postimg FROM posts WHERE id = :postid", array(':postid'=>$_GET['postid']))[0]['postimg'];
                      echo $deleteImg;
                      DB::query('DELETE FROM posts WHERE id=:postid and user_id=:userid', array(':postid'=>$_GET['postid'], ':userid'=>$loggedInUserId));
                      unlink($deleteImg);
                      echo 'Post deleted!';
                  }
              }

              if(isset($_GET['postid']) && !isset($_POST['deletepost']) && !isset($_POST['comment'])){
                  Post::likePost($_GET['postid'], $loggedInUserId);
              }

              if(isset($_GET['postid']) && isset($_POST['comment'])){
                  Comment::createComment($_POST['commentbody'], $_GET['postid'], $loggedInUserId);
              }

              if(isset($_POST['deletecomment'])){
                  DB::query("DELETE FROM comments WHERE id = :commentId", array(':commentId'=>$_GET['commentid']));
              }

        } else {
            die("Error: User doesn't exists!");
        }
    } else {
        die("Error: User is not set!");
    }
}

echo "</p><a href='index.php'>Home</a>
      <a href='profile.php?username=".DB::query("SELECT username FROM users WHERE id = :userid", array(':userid'=>$loggedInUserId))[0]['username']."'>My Profile</a>
      <a href='logout.php'>Logout</a>
      <a href='change-password.php'>Change Password</a><p />";

 ?>

<h1><?php echo $username; ?>'s Profile</h1>
<?php
if($userid == $loggedInUserId){
  echo '
  <form action="profile.php?username='.$username.'" method="post" enctype="multipart/form-data">
    <p>Post something:<br />
    <input type="file" name="postimg"><br />
    <textarea name="postbody" rows="8" cols="60"></textarea>
    <input type="submit" name="post" value="Post">
  </form>';
}
echo "<h1>Posts Timeline:</h1>";
Post::displayPosts($userid, $username, $loggedInUserId, "profile");
?>
