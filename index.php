<?php
include('classes/DB.php');
include('classes/Login.php');
include('classes/Post.php');
include('classes/Comment.php');
include('classes/Image.php');

if(!Login::isLoggedIn()){
    header("location: login.php");
} else {
    $loggedInUserId = Login::isLoggedIn();
    $username = DB::query("SELECT username FROM users WHERE id = :userid", array(':userid'=>$loggedInUserId))[0]['username'];

    if(isset($_GET['postid']) && !isset($_POST['deletepost']) && !isset($_POST['comment'])){
        Post::likePost($_GET['postid'], $loggedInUserId);
    }

    if(isset($_GET['postid']) && isset($_POST['comment'])){
        Comment::createComment($_POST['commentbody'], $_GET['postid'], $loggedInUserId);
    }

    if(isset($_POST['deletecomment'])){
        DB::query("DELETE FROM comments WHERE id = :commentId", array(':commentId'=>$_GET['commentid']));
    }

    echo "Logged in user_id: ".$loggedInUserId."<p />";
    echo "<a href='index.php'>Home</a>
          <a href='profile.php?username=$username'>My Profile</a>
          <a href='logout.php'>Logout</a>
          <a href='change-password.php'>Change Password</a><p />";
    echo " <h1>Home Timeline:</h1>";
    Post::displayPosts(NULL, NULL, $loggedInUserId, "home");
}

?>
<script src="js/jquery.js"></script>
<script src="js/functions.js"></script>
