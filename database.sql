CREATE TABLE `database`.`users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `email` text,
  PRIMARY KEY (`id`)
)

CREATE TABLE `database`.`login_tokens` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `token` CHAR(64),
  `user_id` INT(11) UNSIGNED,
  PRIMARY KEY (`id`)
);

CREATE TABLE `database`.`password_tokens` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `token` CHAR(64),
  `user_id` INT(11) UNSIGNED,
  PRIMARY KEY (`id`)
);

CREATE TABLE `database`.`posts` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `body` VARCHAR(160),
  `posted_at` DATETIME,
  `user_id` INT(11) UNSIGNED,
  `likes` INT(11) UNSIGNED,
  `postimg` VARCHAR(255),
  `topics` VARCHAR(255),
  PRIMARY KEY (`id`)
);

CREATE TABLE `database`.`post_likes` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` INT(11) UNSIGNED,
  `user_id` INT(11) UNSIGNED,
  PRIMARY KEY (`id`)
);

CREATE TABLE `database`.`comments` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment` TEXT NOT NULL,
  `user_id` INT(11),
  `posted_at` DATETIME,
  `post_id` INT(11),
  PRIMARY KEY (`id`)
);
